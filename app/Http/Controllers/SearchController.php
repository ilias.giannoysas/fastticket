<?php

namespace App\Http\Controllers;
use App\Customers;
use App\Tickets;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    public function getView()
    {
        if(Auth::user())
        {
            $to = "'".request('to')."'";
            $from = "'".request('from')."'";
            $status = '*';
            $id = '%';

            if(request('to') == NULL) $to = "'"."9999-12-31"."'";
            if(request('from') == NULL) $from = "'"."1000-01-01"."'";
            if(request('taskid') !== NULL) $id = request('taskid');
            else $id = '%';
            if(request('status') == 'COMPLETED' || request('status') == 'PENDING') $status = request('status');
            else $status = '%';

            // get tasks by customer and date
            $tasks =  DB::table('tasks')
            ->select('tasks.description as task','tasks.id','tasks.visible','tasks.status','customers.name as customer','users.name as user'
            ,DB::raw('convert(tasks.created_at,date) as created_at'))
            ->join('customers', 'customers.id', '=', 'tasks.customer_id')
            ->join('users', 'users.id', '=', 'tasks.user_id')
            ->where('tasks.id','like', $id)
            ->where('customers.name','like', '%'.request('customer').'%')
            ->where('users.name','like', '%'.request('user').'%')
            ->where('tasks.status','like', '%'.$status)
            ->where(function($query){
                $query->where('tasks.user_id','=', auth()->user()->id)
                    ->orWhere('tasks.visible','=','TRUE');
            })            
            ->whereRaw("convert(tasks.created_at,date) >= $from")
            ->whereRaw("convert(tasks.created_at,date) <= $to")
            ->orderByRaw('tasks.created_at DESC','tasks.id')
            ->simplePaginate(10);

            // get total duration
            $totalDuration =  DB::table('tasks')
            ->join('customers', 'customers.id', '=', 'tasks.customer_id')
            ->join('tickets', 'tickets.task_id', '=', 'tasks.id')
            ->join('users', 'users.id', '=', 'tasks.user_id')
            ->where('tasks.id','like', $id)
            ->where('customers.name','like', '%'.request('customer').'%')
            ->where('users.name','like', '%'.request('user').'%')
            ->where('tasks.status','like', '%'.$status)
            ->where(function($query){
                $query->where('tasks.user_id','=', auth()->user()->id)
                    ->orWhere('tasks.visible','=','TRUE');
            })            
            ->whereRaw("convert(tasks.created_at,date) >= $from")
            ->whereRaw("convert(tasks.created_at,date) <= $to")
            ->orderByRaw('tasks.created_at DESC','tasks.id')
            ->sum('tickets.duration');
            
            return view('search',['tasks'=>$tasks,'total'=>$totalDuration]);
        }
        else return redirect('/login');
    }      
}

<?php

namespace App\Http\Controllers;
use App\Customers;
use App\Tickets;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Http\Controllers\Controller;

class SearchTicketsController extends Controller
{
    public function getView()
    {
        if(Auth::user())
        {
            $taskId = request('id');

            // get tickets by customer and date
            $tickets =  DB::table('tickets')
            ->select('tickets.description','tasks.description as task','tasks.visible','tasks.status','tickets.duration','tickets.description','customers.name as customer','users.name as user'
            ,DB::raw('convert(tickets.created_at,date) as created_at'))
            ->join('tasks', 'tasks.id', '=', 'tickets.task_id')
            ->join('customers', 'customers.id', '=', 'tasks.customer_id')
            ->join('users', 'users.id', '=', 'tickets.user_id')
            ->where('tasks.id','=', $taskId)          
            ->orderByRaw('tickets.created_at DESC','tasks.id')
            ->simplePaginate(10);

            // get total duration
            $totalDuration = DB::table('tickets')        
            ->join('tasks', 'tasks.id', '=', 'tickets.task_id')    
            ->join('customers', 'customers.id', '=', 'tasks.customer_id')
            ->join('users', 'users.id', '=', 'tickets.user_id')
            ->where('tasks.id','=', $taskId)                            
            ->sum('tickets.duration');

            return view('ticketssearch',['tickets'=>$tickets,'total'=>$totalDuration]);
        }
        else return redirect('/login');
    }      
}

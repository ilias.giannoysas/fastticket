<?php 
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller {

    public function getView()
    {
        if(Auth::user()) 
        {
            $tasks =  DB::table('tasks')
            ->select('tasks.id','tasks.description','tasks.created_at','users.name as user','customers.name as customer','tasks.visible'
            ,DB::raw('convert(tasks.created_at,date) as created_at'))
            ->join('customers', 'customers.id', '=', 'tasks.customer_id')
            ->join('users','users.id','=','tasks.user_id')
            ->where('tasks.status','=', 'PENDING')
            ->where(function($query){
                $query->where('tasks.user_id','=', auth()->user()->id)
                    ->orWhere('tasks.visible','=','TRUE');
            })  
            ->orderByRaw('tasks.created_at DESC','tasks.id')
            ->simplePaginate(5);
            
            return view("home",["tasks"=>$tasks]);
        }
        else return redirect('/login');
    }  
}
<?php

namespace App\Http\Controllers;
use App\Customers;
use App\Tickets;
use App\Tasks;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TicketsController extends Controller
{
    public function tasks()
    {
        return $this->belongsTo(Tasks::class);
    } 

    public function getView()
    {
        $tasks = DB::table('tasks')->where('status','=','PENDING')->pluck('id');

        if(Auth::user()) return view('tickets',["error"=>"","tasks"=>$tasks]);
        else return redirect('/login');
    }      

    public function store()
    {
        $taskId = DB::table('tasks')
        ->where('id','=', request('taskid'))
        ->value('id');

        if(isset($taskId) && is_numeric(request('duration')))
        {
            Tickets::create([
                'task_id' => $taskId,
                'user_id' => auth()->user()->id,
                'description' => request('description'),
                'duration' => request('duration')
            ]);

            // close task if last ticket
            if(request('closetask') == "TRUE") DB::table('tasks')->where('id',$taskId)->update(['status'=>'COMPLETED']);

            return redirect('/home');
        }
        else 
        {
            $tasks = DB::table('tasks')->pluck('id');
            return view('tickets',["error"=>$tasks,"tasks"=>$tasks]);
        }
    }      
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customers;
use App\Tickets;
use App\Tasks;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TasksController extends Controller
{
    public function tickets()
    {
        return $this->hasMany(Tickets::class);
    } 

    public function user()
    {
        return $this->belongsTo(User::class);
    }     

    public function getView()
    {
        $customers = DB::table('customers')->pluck('name');

        if(Auth::user()) return view('tasks',["error"=>"","customers"=>$customers]);
        else return redirect('/login');     
    }       

    public function store()
    {      
        $customers = DB::table('customers')->pluck('description');

        $customerId = DB::table('customers')
        ->where('name','=', request('cust'))
        ->value('id');

        $taskExist = DB::table('tasks')
        ->where('id','=', request('taskid'))
        ->count();     

        if(isset($customerId) && $taskExist==0 )
        {        
            Tasks::create([
                'user_id' => auth()->user()->id,
                'customer_id' => $customerId,
                'duration' => 0,
                'description' => request('description'),
                'visible' => request('visible')
            ]);
        }
        else return view('tasks',["error"=>"This task already exists for this customer.","customers"=>$customers]);

        return redirect('/home');
    }     


}

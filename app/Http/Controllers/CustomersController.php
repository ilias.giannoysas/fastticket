<?php

namespace App\Http\Controllers;
use App\Customers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CustomersController extends Controller
{
    public function getView()
    {
        if(Auth::user()) return view('customers',["error"=>""]);
        else return redirect('/login');
    }  
    
    public function store()
    {
        $exist = DB::table('customers')
            ->where('name','=',request('name'))
            ->count();

        if($exist == 0 )
        {
            Customers::create([
                'name' => request('name'),
                'description' => request('description'),
            ]);

            return redirect('/home');
        }
        else return view('customers',["error"=>"This customer already exists."]);
    }  

    public function tasks()
    {
        return $this->hasMany(Tasks::class);
    }   
}

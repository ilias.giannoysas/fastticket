<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//LOGIN
Route::get('login', 'Auth\LoginController@getView');
Route::post('login','Auth\LoginController@authenticate');

//REGISTER
Route::get('register','Auth\RegisterController@getView');
Route::post('register','Auth\RegisterController@store');

//HOME
Route::get('home', 'HomeController@getView');

//SEARCH TASKS
Route::get('search', 'SearchController@getView');

//SEARCH TICKETS
Route::get('searchTickets/{id}', 'SearchTicketsController@getView');

//CUSTOMERS
Route::get('AddCustomer','CustomersController@getView');
Route::post('AddCustomer','CustomersController@store');

//TASKS
Route::get('AddTask','TasksController@getView');
Route::post('AddTask','TasksController@store');

//TICKETS
Route::get('AddTicket','TicketsController@getView');
Route::post('AddTicket','TicketsController@store');

//LOGOUT
Route::get('logout','Auth\LoginController@logout');

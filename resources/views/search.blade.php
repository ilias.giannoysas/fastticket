<!DOCTYPE html>
<html lang="en">

<head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>FastTicket</title>
</head>

<body id="page-top" class="bg-light">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="/home">FastTicket</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
     
        <ul class="navbar-nav ml-auto">
            @auth
              <li class="nav-item pl-3"><a href="{{ url('/home') }}" class="text-white">Home</a></li>
              <li class="nav-item pl-3"><a href="{{ url('/logout') }}" class="text-white">Logout</a></li>
            @else
              <li class="nav-item pl-3"><a href="{{ url('/login') }}" class="text-white">Login</a></li>
              <li class="nav-item pl-3"><a href="{{ url('/register') }}"  class="text-white">Register</a></li>
            @endauth
        </ul>
      </div>
    </div>
  </nav>
    
<div class="container pt-5 pb-5 mt-5">
    <h1>Tasks</h1>
    <div class="row">
      <div class="col">
      <table class="table table-bordered">
          <thead class="thead-dark">
            <tr>
              <th scope="col">Task ID</th>
              <th scope="col">Date Created</th>
              <th scope="col">For Customer</th>
              <th scope="col">From User</th>
              <th scope="col">Task Description</th> 
              <th scope="col">Task Status</th>
              <th scope="col">Tickets</th>
            </tr>
          </thead>
          <tbody>
              @foreach($tasks as $task)
                  @if($task->visible == 'FALSE') <tr class="table-warning">
                  @elseif($task->status == 'COMPLETED') <tr class="table-success">
                  @else <tr class="table-secondary">
                  @endif         
                <td>{{ $task->id }}</td>     
                <td>{{ $task->created_at }}</td>
                <td>{{ $task->customer }}</td>
                <td>{{ $task->user }}</td>
                <td>{{ $task->task }}</td>
                <td>{{ $task->status }}</td>
                <td> <a href="searchTickets/{{$task->id}}">View</a></td>
              </tr>
              @endforeach
          
          </tbody>
        </table>  
        {{ $tasks->links() }}
        <p class="d-flex justify-content-center lead">
          @if(($total/60)>1) Total Duration : {{ floor($total/60) }} hours 
            @if(($total%60)>1) and {{ $total%60 }} minutes @endif
          @else Total Duration : {{ $total }} minutes 
          @endif 
        </p>
      </div>
    </div>   
</div>      

</body>
</html>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
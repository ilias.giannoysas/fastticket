<!DOCTYPE html>
<html lang="en">

<head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>FastTicket</title>
</head>

<body id="page-top" class="bg-light">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="/home">FastTicket</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
     
        <ul class="navbar-nav ml-auto">
            @auth
              <li class="nav-item pl-3"><a href="{{ url('/home') }}" class="text-white">Home</a></li>
              <li class="nav-item pl-3"><a href="{{ url('/logout') }}" class="text-white">Logout</a></li>
            @else
              <li class="nav-item pl-3"><a href="{{ url('/login') }}" class="text-white">Login</a></li>
              <li class="nav-item pl-3"><a href="{{ url('/register') }}"  class="text-white">Register</a></li>
            @endauth
        </ul>
      </div>
    </div>
  </nav>

    <div class="container pt-5 pb-5 mt-5">
      <div class="row">
        <div class="col-sm">
        <h2>Welcome {{ auth()->user()->name }}</h2>
          <p class="lead">What you are gonna do today?
          </p>
        </div>
        <div class="col-sm pt-4">
          <ul>
              <li><a class="text-dark" href="/AddCustomer">Add customer</a></li>
              <li><a class="text-dark" href="/AddTask">Add task</a></li>
              <li><a class="text-dark" href="/AddTicket">Add ticket</a></li>
          </ul>
        </div>
      </div>

      <p class="lead pt-4"><h4>Advanced Search</h4></p>
      <hr>
      <div class="row">
        <div class="col">
          <form method="GET" action="/search">
            <div class="form-group d-flex justify-content-start">
                {{ csrf_field() }}
                <div class="mr-2">
                <label for="exampleInputName1"><strong>Customer :</strong></label>
                  <input type="text" name="customer" class="form-control" id="exampleInputName1" aria-describedby="NameHelp">
                </div>    
                <div class="mr-2">
                <label for="exampleInputName1"><strong>User :</strong></label>
                  <input type="text" name="user" class="form-control" id="exampleInputUser1" aria-describedby="UserHelp">
                </div>                
                <div class="mr-2">
                    <label for="exampleInputEmail1"><strong>Task ID:</strong></label>
                    <input type="text" name="taskid" class="form-control" id="exampleInputFrom1" aria-describedby="TaskHelp">
                </div>          
                <div class="mr-2">
                  <div class="from-group">
                      <label for="exampleInputEmail1"><strong>Status : </strong></label> 
                      <select name="status" class="form-control" id="status">
                          <option value="ALL"></option>
                          <option value="PENDING">pending</option>
                          <option value="COMPLETED">completed</option>
                      </select>
                  </div> 
                </div>                                   
                <div class="mr-2">
                  <label for="exampleInputEmail1"><strong>From :</strong></label>
                  <input type="date" name="from" class="form-control" id="exampleInputFrom1" aria-describedby="fromHelp">
                </div>
                <div class="mr-2">
                  <label for="exampleInputPassword1"><strong>To :</strong></label>
                  <input type="date" name="to" class="form-control" id="exampleInputTo1" placeholder="To">
                </div>             
                <div class="d-flex justify-content-center mt-4"><button type="submit" class="btn btn-primary">Search</button></div>
            </div>            
          </form>  
        </div>      
      </div>    
               
      @if($tasks != null)
        <p class="pt-4"><h4>Pending Tasks</h4>
        <div class="d-flex justify-content-start">
            <div class="alert alert-dark col-2" role="alert">Visible Tasks</div>
            <div class="alert alert-warning col-2 ml-2" role="alert">Invisible Tasks</div>
        </div>
    <div class="row">
      <div class="col">
          <table class="table table-bordered">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">Task ID</th>
                  <th scope="col">Date Created</th>
                  <th scope="col">For Customer</th>
                  <th scope="col">From User</th>
                  <th scope="col">Task Description</th>
                </tr>
              </thead>
              <tbody>

              @foreach($tasks as $task)
                @if($task->visible == 'FALSE') <tr class="table-warning">
                @else <tr class="table-secondary">
                @endif
                <td>{{ $task->id }}</td>
                <td>{{ $task->created_at }}</td>
                <td>{{ $task->customer }}</td>
                <td>{{ $task->user }}</td>
                <td>{{ $task->description }}</td>
              </tr>
              @endforeach

              </tbody>
          </table>
        </div>         
      </div>
      {{ $tasks->links() }}
      @endif
  
    </div>
</body>
</html>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
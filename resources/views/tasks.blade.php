<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Add Ticket</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
     <div>
        <div class="row d-flex justify-content-center">
            <div class="col-6">
                <form class="border p-3 mt-5 shadow-lg p-3 mb-5 bg-white rounded" method="POST" action="/AddTask">
                    {{ csrf_field() }}

                    <div class="pb-3"><h3>Add new task :</h3></div>
                    <div class="form-group">
                        <label for="exampleInputName1"><strong>Customer :</strong></label>
                        <div class="input-group mb-3">
                            <select name="cust" class="form-control"  id="customerId" required>
                                <option selected>Choose customer</option>
                                {@if($customers)
                                    @foreach($customers as $customer)
                                        <option value="{{ $customer }}">{{ $customer }}</option>
                                    @endforeach 
                                @endif}
                            </select>
                        </div>                             
                    </div>          
                    <label for="exampleInputName1"><strong>Description :</strong></label>                                       
                    <div class="form-group">
                        <textarea class="form-control" name="description" id="exampleFormControlTextarea1" placeholder="Enter description" rows="5" required></textarea>
                    </div>
                    <label for="exampleInputName1"><strong>Visible for others :</strong></label>
                    <div class="from-group">
                        <div class="input-group mb-3">
                            <select name="visible" class="form-control col-sm-2"  id="visible" required>
                                <option value="TRUE">Yes</option>
                                <option value="FALSE">No</option>
                            </select>
                        </div>                         
                    </div>
                    @if ($error)
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                    @endif                     
                    <div class="d-flex justify-content-center"><button type="submit" class="btn btn-primary mt-2">Add Task</button></div>                   
                </form>                 
            </div>
        </div>
    </div>
    </body>


</html>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

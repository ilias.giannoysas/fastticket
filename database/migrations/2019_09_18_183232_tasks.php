<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('customer_id'); 
            $table->unsignedBigInteger('user_id');            
            $table->text('description');  
            $table->integer('duration')->default(0); 
            $table->string('status')->default('PENDING');   
            $table->string('visible')->default('TRUE');
            $table->timestamps();   
            
            $table->index('customer_id');               
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
